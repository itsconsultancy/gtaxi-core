﻿using GTaxi.Helper;
using Microsoft.Owin.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Psgn.ApiServer
{
    class Program
    {
        // ปรับ setting พวกนี้เอาไว้ใน App.config ไฟล์
        static bool isDebugMode = true;
        static string serverProtocol = "http";
        static string serverEndPoint = "*";
        static int serverPort = 11260;

        static void Main(string[] args)
        {
            // Pre-configuration
            //ServerConfigration();

            // Server base address
            if (isDebugMode)
            {
                serverEndPoint = "localhost";
                serverPort = 8080;
            }
            string baseAddress = string.Format("{0}://{1}:{2}/", serverProtocol, serverEndPoint, serverPort);

            // Run server
            Console.Title = "Psgn Api Server : " + serverPort.ToString();
            ConsoleLog.WriteLog("Server", "Starting web Server...", ConsoleColor.White);
            try
            {
                // Start server
                WebApp.Start<Startup>(baseAddress);

                //Task.Factory.StartNew(() =>
                //{
                //    try
                //    {
                //        System.Threading.Thread.Sleep(1000);
                //        ConsoleLog.WriteLog("", "Begin Testing", ConsoleColor.Cyan);
                //        for (int i = 0; i < 5; i++)
                //        {
                //            var cts = new CancellationTokenSource(TimeSpan.FromSeconds(10));
                //            var client = new RestClient(baseAddress)
                //            {
                //                Timeout = 1000
                //            };
                //            var request = new RestRequest("test/" + (i + 1), Method.GET)
                //            {
                //                Timeout = 1000
                //            };
                //            //client.ExecuteAsync(request, A);
                //            client.Execute(request);
                //        }
                //    }
                //    catch
                //    {
                //        ConsoleLog.WriteLog("kak", "sad", ConsoleColor.Red);
                //    }
                //});
            }
            catch (Exception ex)
            {
                ConsoleLog.WriteLog("Server", ex.ToString(), ConsoleColor.Red);
                Console.ReadKey();
                Environment.Exit(0);
            }

            ConsoleLog.WriteLog("Server", "Server running at " + baseAddress, ConsoleColor.Green);
            Console.ReadLine();
        }

        // Pre-configuration
        static void ServerConfigration()
        {
            // Config Redis Database

            //// Config mapper
            //Mapper.CreateMap<InfrastNamespace.Traffic, DTOs.TrafficDataDTO>();
            //Mapper.CreateMap<InfrastNamespace.TrafficLog, DTOs.TrafficDataDTO>();
            //Mapper.CreateMap<InfrastNamespace.DeviceInfo, DTOs.DeviceInfoDTO>();

            //// Validate mapper
            //try
            //{
            //    Mapper.AssertConfigurationIsValid();
            //}
            //catch
            //{
            //    Console.WriteLine("Mapper configuration failed");
            //    Console.ReadKey();
            //    Environment.Exit(0);
            //}
        }
    }
}
