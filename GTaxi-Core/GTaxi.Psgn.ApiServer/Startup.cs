﻿using Microsoft.Owin.Host.HttpListener;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace GTaxi.Psgn.ApiServer
{
    public class Startup
    {
        // This method is required by Katana:
        public void Configuration(IAppBuilder app)
        {
            var webApiConfiguration = ConfigureWebApi();

            var owinHttpListener = (OwinHttpListener)app.Properties["Microsoft.Owin.Host.HttpListener.OwinHttpListener"];
            owinHttpListener.SetRequestProcessingLimits(1, 1);

            // Use the extension method provided by the WebApi.Owin library:
            app.UseWebApi(webApiConfiguration);
        }

        // Routing Configuration
        private HttpConfiguration ConfigureWebApi()
        {
            var config = new HttpConfiguration();

            // Template routing
            //config.Routes.MapHttpRoute(
            //    "DefaultApi",
            //    "api/{controller}");

            //  Enable attribute based routing
            config.MapHttpAttributeRoutes();

            return config;
        }
    }
}
