﻿using ITS.Server.Infrast.AttrExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace GTaxi.Psgn.ApiServer.Controllers
{
    [AllowCrossSiteJson]
    [RoutePrefix("taxi")]
    public class TaxiController : ApiController
    {
        [HttpGet]
        [Route("getNearBy/{uid:int}/{lat:double}-{lng:double}")]
        public HttpResponseMessage GetNearBy(int uid, double lat, double lng)
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}
