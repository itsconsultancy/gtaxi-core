﻿using System;

namespace GTaxi.Helper
{
    /// <summary>
    /// เขียน Log ออก CMD Console
    /// </summary>
    public class ConsoleLog
    {
        #region Logging
        public static void WriteLog(string processName, string message, ConsoleColor messageColor)
        {
            Console.Write(DateTime.Now.ToString("HH:mm:ss") + " ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(processName);
            Console.ForegroundColor = messageColor;
            Console.WriteLine(" " + message);
            Console.ResetColor();
        }

        private static object syncObj = new object();
        public static void WriteLogSync(string processName, string message, ConsoleColor messageColor)
        {
            lock (syncObj)
            {
                Console.Write(DateTime.Now.ToString("HH:mm:ss") + " ");
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.Write(processName);
                Console.ForegroundColor = messageColor;
                Console.WriteLine(" " + message);
                Console.ResetColor();
            }
        }
        #endregion
    }
}
