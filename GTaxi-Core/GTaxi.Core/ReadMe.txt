﻿================== ความหมายศัพท์
1. Definitions : หมายถึง enum definition
2. Interfaces : มี 2 ชนิด
	2.1 Interfaces ทั่วไป มีทั้ง property และ method จะใช้คำว่า "Interfaces"
	2.2 Interfaces สำหรับรูปลักษณ์ (Aspect) ของข้อมูล (มีเฉพาะ property) จะใช้คำว่า "Interfaces.Data"
3. Models : หมายถึง คลาสที่ใช้ในการเก็บข้อมูล (มีเฉพาะ property เท่านั้น)
4. CCS : Central Control System (ระบบส่วนกลาง)

================== โครงสร้างโปรเจค GTaxi.Core [ส่วน Definition]

Definitions

	ใช้สำหรับกำหนด enum definition ต่าง เป้าหมายคือ mapping meaningful word with tedious numeric number
	โดยตัวเลขที่ mapping นิยามต่าง ๆ จะเป็นตัวเลขจริงที่ใช้เก็บลงในฐานข้อมูล

	โครงสร้างส่วนนี้ไม่มีความซับซ้อน ใช้แค่ hierarchy level เดียว

================== โครงสร้างโปรเจค GTaxi.Core [ส่วน Interfaces]

1. Interfaces -> Api -> Taxi (Interfaces.Api.Taxi)

	หมายถึง Interfaces ในส่วน Api ที่่ติดต่อกับ Taxi

	Interface ที่กำหนดขึ้นในนี้จะใช้เป็น Web Api สำหรับรับส่งข้อมูลระหว่าง CCS และ Taxi

2. Interfaces -> Data -> Taxi (Interfaces.Data.Taxi)

	หมายถึง Interfaces ที่แสดงคุณลักษณะเชิงข้อมูลต่าง ๆ ของ Taxi ในมุมมองที่ CCS เห็น Taxi

	Interface ที่กำหนดขึ้นในนี้คือองค์ประกอบต่าง ๆ ของข้อมูลที่ CCS เห็น Taxi 
	(ในความเป็นจริง Taxi อาจมีข้อมูลมากกว่าเพื่อให้ระบบภายใน Taxi ทำงานได้สมบูรณ์ แต่อย่างไรก็ตาม CCS จะไม่เห็นข้อมูลเหล่านั้น CCS จะเห็นแค่ข้อมูลที่กำหนดใน Interfaces.Data.Taxi เท่านั้น)
	กล่าวอีกนัยหนึ่งก็คือ Interfaces.Data.Taxi ใช้กำหนด public property ของ Taxi
	
================== โครงสร้างโปรเจค GTaxi.Core [ส่วน Models]

1. Models -> Api -> Taxi

	หมายถึง Models (POCO, DTO) ที่ใช้ส่งในระดับ Api ระหว่าง CCS และ Taxi

	Model ที่กำหนดขึ้นในนี้จะใช้เป็น Request, Response Object สำหรับ Interfaces.Api.Taxi
	Model ในนี้จะถูกประกอบขึ้นจาก Interfaces.Data.Taxi