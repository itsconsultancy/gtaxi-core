﻿using GTaxi.Core.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Models.TaxiApi
{
    public class SignInRequest
    {
        public int TaxiID { get; set; }

        public int DriverID { get; set; }

        public int JobID { get; set; }

        public double? Lat { get; set; }
        public double? Lng { get; set; }

        public TaxiServiceStatus ServiceStatus { get; set; }
        public TaxiOutServiceStatus OutServiceStatus { get; set; }
    }

    public class SignInResponse
    {
        public List<BroadcastJobInfo> JobList { get; set; }
    }

    public class BroadcastJobInfo
    {
        public int JobID { get; set; }
        public DateTime CreatedTime { get; set; }

        public double PickLat { get; set; }
        public double PickLng { get; set; }
        public string PickLocation { get; set; }
        public string PickLandmark { get; set; }
        public DateTime? PickExpectedTime { get; set; }

        public double? DestLng { get; set; }
        public double? DestLat { get; set; }
        public string DestLocation { get; set; }
        public DateTime? DestExpectedTime { get; set; }
    }
}
