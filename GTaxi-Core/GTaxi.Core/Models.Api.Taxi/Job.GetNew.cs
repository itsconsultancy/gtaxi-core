﻿using GTaxi.Core.Interfaces.Data.Taxi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Models.Api.Taxi
{
    public class GetNewJobRequest : ITaxi, IDriver, IJob
    {
        public int TaxiID { get; set; }
        public int DriverID { get; set; }
        public int JobID { get; set; }
    }

    /// <summary>
    /// Get New Job ส่งเฉพาะข้อมูล Passenger ที่จำเป็นเท่านั้น
    /// เนื่องจากข้อมูล O-D Job ถูกส่งไปขั้นตอน Boardcast แล้ว ไม่จำเป็นต้องส่งซ้ำอีก
    /// </summary>
    public class GetNewJobResponse : IPsgn { }
}
