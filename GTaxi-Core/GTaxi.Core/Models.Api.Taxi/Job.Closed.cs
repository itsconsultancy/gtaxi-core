﻿
using GTaxi.Core.Interfaces.Data.Taxi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTaxi.Core.Definitions;

namespace GTaxi.Core.Models.Api.Taxi
{
    public class PushClosedJobRequest : IJob, IJobTracking, IJobCost
    {
        public int JobID { get; set; }

        // Job Accepted
        public DateTime? JAccepted { get; set; }
        public double JAcceptedLat { get; set; }
        public double JAcceptedLng { get; set; }

        // Arrive O
        public DateTime? ArrPick { get; set; } // Arrived pick-up point (also refer to arrive passenger)
        public double ArrPickLat { get; set; }
        public double ArrPickLng { get; set; }

        // Arrive D
        public DateTime? ArrDest { get; set; }
        public double ArrDestLat { get; set; }
        public double ArrDeskLng { get; set; }

        // Job Closed
        public DateTime? JClosed { get; set; }
        public double JClosedLat { get; set; }
        public double JClosedLng { get; set; }

        // Job Cost
        public decimal TotalCost { get; set; }
        public PaymentType PaymentType { get; set; }
        public decimal Discount { get; set; }
        public PromotionUnit PromotionUnit { get; set; }
    }

}
