﻿using GTaxi.Core.Interfaces.Data.Taxi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Models.Api.Taxi
{
    public class LoginRequest : IDriver
    {
        #region Driver Info
        public int DriverID { get; set; }
        #endregion

        #region Credentials
        public string Password { get; set; }
        public string Reference { get; set; }
        #endregion
    }

    public class LoginResponse
    {
        public bool Success { get; set; }
        public string Token { get; set; } // Unique Secret Token
    }

    public class LogoutRequest : IDriver
    {
        public int DriverID { get; set; }
    }
}
