﻿using GTaxi.Core.Interfaces.Data.Taxi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTaxi.Core.Definitions;

namespace GTaxi.Core.Models.Api.Taxi
{
    public class AskForJobRequest : ITaxi, IDriver, ILocation, ITaxiServiceStatus
    {
        public int TaxiID { get; set; }
        public int DriverID { get; set; }

        public double Lat { get; set; }
        public double Lng { get; set; }

        public TaxiServiceStatus ServiceStatus { get; set; }
    }

    public class AskForJobResponse
    {
        IEnumerable<JobInfoBasedAsking> JobList { get; set; }
    }
    
    /// //////////////////////////////////////

    public class JobInfoBasedAsking : IJob, IJobOrigin, IJobDestination
    {
        public int JobID { get; set; }

        // O
        public DateTime? PickExpectedTime { get; set; }
        public double PickLat { get; set; }
        public double PickLng { get; set; }
        public string PickLandmark { get; set; }
        public string PickLocation { get; set; }

        // D
        public string DeskLocation { get; set; }
        public double? DestLat { get; set; }
        public double? DestLng { get; set; }
        public DateTime? DestExpectedTime { get; set; }
        
    }

}
