﻿using GTaxi.Core.Interfaces.Data.Taxi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Models.Api.Taxi
{
    public class AckNewJobRequest : IDriver, ITaxi, IJob
    {
        public int TaxiID { get; set; }
        public int DriverID { get; set; }
        public int JobID { get; set; }
    }
}
