﻿using GTaxi.Core.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Data.Taxi
{
    /// <summary>
    /// Interface : กำหนดข้อมูลพื้นฐานของ Job
    /// </summary>
    public interface IJob
    {
        int JobID { get; set; }
    }

    /// <summary>
    /// Interface : กำหนดข้อมูลเกี่ยวกับตำแหน่งต้นทางของ Job
    /// </summary>
    public interface IJobOrigin
    {
        DateTime? PickExpectedTime { get; set; } // เวลาที่ต้องการให้ไปถึงจุดรับ (ถ้ามี)
        double PickLat { get; set; }
        double PickLng { get; set; } 
        string PickLocation { get; set; } // อธิบาย ตำแหน่งจุดรับ
        string PickLandmark { get; set; } // อธิบาย จุดสังเกต
    }

    /// <summary>
    /// Interface : กำหนดข้อมูลเกี่ยวกับตำแหน่งปลายทางของ Job
    /// </summary>
    public interface IJobDestination
    {
        DateTime? DestExpectedTime { get; set; } // เวลาที่ต้องการให้ไปถึงปลายทาง (ถ้ามี)
        double? DestLat { get; set; }
        double? DestLng { get; set; }
        string DeskLocation { get; set; } // อธิบาย ตำแหน่งจุดปลายทาง
    }

    /// <summary>
    /// Interface : กำหนดข้อมูลการติดตาม Job
    /// </summary>
    public interface IJobTracking
    {
        // Job Accepted
        DateTime? JAccepted { get; set; }
        double JAcceptedLat { get; set; }
        double JAcceptedLng { get; set; }

        // Arrive O
        DateTime? ArrPick { get; set; } // Arrived pick-up point (also refer to arrive passenger)
        double ArrPickLat { get; set; }
        double ArrPickLng { get; set; }
        
        // Arrive D
        DateTime? ArrDest { get; set; }
        double ArrDestLat { get; set; }
        double ArrDeskLng { get; set; }

        // Job Closed
        DateTime? JClosed { get; set; }
        double JClosedLat { get; set; }
        double JClosedLng { get; set; }
    }

    /// <summary>
    /// Interface : กำหนดข้อมูลการติดตาม Job
    /// ====
    /// IPromotion : มี feature [Promotion]
    /// </summary>
    public interface IJobCost : IPromotion
    {
        decimal TotalCost { get; set; }
        PaymentType PaymentType { get; set; }
    }

}
