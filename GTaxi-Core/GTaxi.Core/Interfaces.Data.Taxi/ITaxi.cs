﻿using GTaxi.Core.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Data.Taxi
{
    /// <summary>
    /// Interface : กำหนดข้อมูลพื้นฐานของ Taxi
    /// </summary>
    public interface ITaxi
    {
        int TaxiID { get; set; }
    }

    public interface ITaxiServiceStatus
    {
        TaxiServiceStatus ServiceStatus { get; set; }
    }

    public interface ITaxiOutServiceStatus
    {
        TaxiOutServiceStatus OutServiceStatus { get; set; }
    }

}
