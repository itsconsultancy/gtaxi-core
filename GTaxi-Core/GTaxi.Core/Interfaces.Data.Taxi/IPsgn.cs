﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Data.Taxi
{
    /// <summary>
    /// Interface : กำหนดข้อมูลพื้นฐานของ Passenger
    /// </summary>
    public class IPsgn
    {
        int PID { get; set; }
    }

    /// <summary>
    /// Interface : กำหนดข้อมูลของ Passenger
    /// </summary>
    public class IPsgnInfo : IPsgn
    {
        string FName { get; set; }
        string LName { get; set; }
        string PhoneNO { get; set; }
        string ImageURL { get; set; }
    }
}
