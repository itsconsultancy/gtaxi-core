﻿using GTaxi.Core.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Data.Taxi
{
    /// <summary>
    /// Interface : กำหนดข้อมูลเกี่ยวกับ Promotion
    /// </summary>
    public interface IPromotion
    {
        decimal Discount { get; set; } // จำนวนที่ลด
        PromotionUnit PromotionUnit { get; set; } // หน่วยที่ลด
    }
}
