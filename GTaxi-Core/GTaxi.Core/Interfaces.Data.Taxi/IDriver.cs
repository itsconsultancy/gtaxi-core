﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Data.Taxi
{
    /// <summary>
    /// Interface : กำหนดข้อมูลพื้นฐานของ Driver
    /// </summary>
    public interface IDriver
    {
        int DriverID { get; set; }
    }

    /// <summary>
    /// Interface : ข้อมูล Driver
    /// </summary>
    public interface IDriverInfo : IDriver
    {
        string FName { get; set; } // Firstname
        string LName { get; set; } // Lastname
        string PhoneNO { get; set; } // Phone Number

    }
}
