﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Data.Taxi
{
    /// <summary>
    /// Interface : กำหนดข้อมูลเกี่ยวกับ Geo Location
    /// </summary>
    public interface ILocation
    {
        double Lat { get; set; }
        double Lng { get; set; }
    }
}
