﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Data.Taxi
{
    /// <summary>
    /// Interface : กำหนดข้อมูลที่ Taxi ต้องใช้ในการเข้ามาเช็คชื่อกับ CCS
    /// ====
    /// ใช้ในกระบวนการเช็คชื่อ (Taxi Sign-in Process)
    /// ====
    /// ประกอบด้วย
    /// 1. ITaxi: TID
    /// 2. ILocation: ข้อมูลตำแหน่ง
    /// 3. ITaxiServiceStatus: ข้อมูลสถานะ Taxi
    /// </summary>
    public interface ITaxiSignIn : ITaxi, ILocation, ITaxiServiceStatus { }
}
