﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Definitions
{
    /// <summary>
    /// สถานะการให้บริการ (service) ของรถ Taxi
    /// </summary>
    public enum TaxiServiceStatus
    {
        OutOfService = 1, // ไม่อยู่ในสถานะให้บริการ
        StandBy = 2, // ว่าง
        Reserved = 3, // ถูกจอง
        OnJob = 4 // รับผดส.แล้ว
    }

    /// <summary>
    /// สถานะไม่สามารถให้บริการได้ (out of service) ของรถ Taxi
    /// </summary>
    public enum TaxiOutServiceStatus
    {
        None = 0, // Taxi ทำงานปกติ (Default)
        OnBreak = 1, // ไม่อยู่ในสถานะให้บริการ (ทำธุระ)
        Maintenance = 2, // ไม่อยู่ในสถานะให้บริการ (ซ่อมรถ)
        Emergency = 3, // ไม่อยู่ในสถานะให้บริการ (เหตุฉุกเฉิน)
        Retired = 4 // --------
    }
}
