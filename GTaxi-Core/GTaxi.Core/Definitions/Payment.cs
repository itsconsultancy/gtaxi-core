﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Definitions
{
    /// <summary>
    /// รูปแบบการจ่ายเงิน
    /// </summary>
    public enum PaymentType
    {
        None = 0,
        MeterRate = 1, // มิเตอร์ (Default)
        FlatRate = 2 // เหมา
    }

    /// <summary>
    /// ช่องทางการจ่ายเงิน
    /// </summary>
    public enum PaymentChannel
    {
        None = 0,
        Cash = 1, // เงินสด
        CreditCard = 2 // Credit card
    }
}
