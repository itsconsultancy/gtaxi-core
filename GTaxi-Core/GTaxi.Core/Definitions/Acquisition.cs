﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Definitions
{
    /// <summary>
    /// รูปแบบการรับผู้โดยสาร
    /// </summary>
    public enum AcquisitionType
    {
        None = 0,
        Thumbing = 1, // โบก
        Mobile = 2, // จองผ่านมือถือ
        Web = 3, // จองผ่านเว็บ
        CallCenter = 4 // จองผ่าน Call Center
    }
}
