﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Definitions
{
    /// <summary>
    /// หน่วยสำหรับคำนวณ Promotion
    /// </summary>
    public enum PromotionUnit
    {
        Unit = 1, // ลดเป็น unit เงิน
        Percent = 2 // ลดเป็น %
    }
}
