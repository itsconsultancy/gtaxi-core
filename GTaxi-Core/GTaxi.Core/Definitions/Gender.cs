﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Definitions
{
    /// <summary>
    /// เพศ
    /// </summary>
    public enum Gender
    {
        Male = 1,
        Female = 2,
        Gay = 3,
        SheMale = 4,
        Lesbian = 5
    }
}
