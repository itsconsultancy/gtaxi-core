﻿using GTaxi.Core.Definitions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Models.Api.Tmc
{
    public class TaxiInfo
    {
        /// <summary>
        /// Taxi ID
        /// </summary>
        public int TID { get; set; }

        /// <summary>
        /// Job ID
        /// </summary>
        public int? JID { get; set; }

        #region Driver Info
        /// <summary>
        /// Driver ID
        /// </summary>
        public int DID { get; set; }
        /// <summary>
        /// Firstname
        /// </summary>
        public string FName { get; set; }
        /// <summary>
        /// Lastname
        /// </summary>
        public string LName { get; set; }
        /// <summary>
        /// Gender
        /// </summary>
        public Gender Gender { get; set; }
        /// <summary>
        /// Phone number
        /// </summary>
        public string PhoneNO { get; set; }
        /// <summary>
        /// Driver image url
        /// </summary>
        public string ImgURL { get; set; }
        #endregion
        
        #region Location
        /// <summary>
        /// Current Latitude
        /// </summary>
        public double Lat { get; set; }
        /// <summary>
        /// Current Longitude
        /// </summary>
        public double Lng { get; set; }
        #endregion

        #region Status
        /// <summary>
        /// เวลาที่ Taxi เข้ามารายงานตัวกับ CCS
        /// </summary>
        public DateTime? SignedTime { get; set; }

        /// <summary>
        /// เวลาที่ ServiceStatus เปลี่ยนล่าสุด
        /// </summary>
        public DateTime? ServiceStatusModified { get; set; }

        /// <summary>
        /// สถานะการให้บริการ (service status)
        /// </summary>
        TaxiServiceStatus ServiceStatus { get; set; }

        /// <summary>
        /// เวลาที่ OutServiceStatus เปลี่ยนล่าสุด
        /// </summary>
        public DateTime? OutServiceStatusModified { get; set; }

        /// <summary>
        /// ชนิดสถานะเมื่อ Taxi ไม่สามารถให้บริการได้ (out of service status)
        /// </summary>
        TaxiOutServiceStatus OutServiceStatus { get; set; }
        #endregion
    }
}
