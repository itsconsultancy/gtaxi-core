﻿using GTaxi.Core.Models.Api.Taxi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Api.Taxi
{
    /// <summary>
    /// Interface : สำหรับ Driver Access Process
    /// </summary>
    public interface IDriverAccessApi
    {
        // Api : สำหรับ Driver Login
        LoginResponse Login(LoginRequest request);

        // Api : สำหรับ Driver Logout
        bool Logout(LogoutRequest request);
    }
}
