﻿using GTaxi.Core.Models.TaxiApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Api.Taxi
{
    /// <summary>
    /// Interface : สำหรับกระบวนการ Taxi Sign-in
    /// </summary>
    public interface ITaxiSignInApi
    {
        // Api : สำหรับ Taxi Sign-in process
        SignInResponse SignIn(SignInRequest request);
    }
}
