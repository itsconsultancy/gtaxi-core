﻿using GTaxi.Core.Models.Api.Taxi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Core.Interfaces.Api.Taxi
{
    /// <summary>
    /// Interface : กำหนด Job Api
    /// </summary>
    public interface IJobApi
    {
        /// <summary>
        /// Api : สำหรับดาวน์โหลด Boardcast Job
        /// </summary>
        AskForJobResponse AskForJob(AskForJobRequest request);

        /// <summary>
        /// Api : สำหรับดาวน์โหลด New Job
        /// ====
        /// ใช้เมื่อ driver กดตกลงเพื่อรับ job
        /// </summary>
        GetNewJobResponse GetNewJob(GetNewJobRequest request);

        /// <summary>
        /// Api : ยืนยันการได้ New Job
        /// ====
        /// ใช้หลังจาก GetNewJob() เพื่อ ack กับ CCS ว่าข้อมูล New Job ถึง driver แล้วจริง ๆ
        /// </summary>
        bool AckNewJob(AckNewJobRequest request);

        /// <summary>
        /// Api : ใช้เพื่ออัพโหลดข้อมูล Job ที่จบแล้ว
        /// </summary>
        bool PushClosedJob(PushClosedJobRequest request);
    }
}
