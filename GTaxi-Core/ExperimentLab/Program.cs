﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExperimentLab
{
    class Program
    {
        private static Random rand = new Random();
        static void Main(string[] args)
        {
            RedisConnector.Init();

            var db = RedisConnector.GetDatabase();
            var utid = db.StringIncrement("next_taxi_id");

            Taxi taxi = new Taxi();
            taxi.ID = (int)utid;
            taxi.Lat = rand.NextDouble() * 13;
            taxi.Lng = rand.NextDouble() * 100;
            db.CreateTransaction();
   
            db.HashSet("taxi:" + utid, "a", 1);

            Console.ReadLine();

        }
    }

    public class Taxi
    {
        public int ID { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
    }
}
