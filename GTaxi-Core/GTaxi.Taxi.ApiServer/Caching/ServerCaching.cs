﻿using GTaxi.Core.Definitions;
using GTaxi.Core.Models.TaxiApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Taxi.ApiServer.Caching
{
    public class ServerCaching
    {
        public static Dictionary<int, RegisteredTaxi> _RegisteredTaxiList { get; set; } = new Dictionary<int, RegisteredTaxi>();
        public static Dictionary<int, RegisteredDriver> _RegisteredDriverList { get; set; } = new Dictionary<int, RegisteredDriver>();

        public static object registeredTaxiLockObj = new object();
        public static object registeredDriverLockObj = new object();

        public static void UpdateRegisteredTaxi(RegisteredTaxi taxi)
        {
            lock (registeredTaxiLockObj)
            {
                if (_RegisteredTaxiList.ContainsKey(taxi.TaxiID))
                {
                    _RegisteredTaxiList[taxi.TaxiID] = taxi;
                }
                else
                {
                    _RegisteredTaxiList.Add(taxi.TaxiID, taxi);
                }
            }
        }
        public static List<BroadcastJobInfo> GetBroadcastJob(RegisteredTaxi taxi)
        {
            if (!_RegisteredTaxiList.ContainsKey(taxi.TaxiID)) return null;
            return _RegisteredTaxiList[taxi.TaxiID].JobList;
        }

        public static void UpdateRegisteredDriver(RegisteredDriver driver)
        {
            lock (registeredTaxiLockObj)
            {
                if (_RegisteredDriverList.ContainsKey(driver.ID))
                {
                    _RegisteredDriverList[driver.ID] = driver;
                }
                else
                {
                    _RegisteredDriverList.Add(driver.ID, driver);
                }
            }
        }
    }
    public class RegisteredTaxi
    {
        #region Taxi updates
        public int TaxiID { get; set; }

        public int DriverID { get; set; }

        public int JobID { get; set; }

        public double? Lat { get; set; }
        public double? Lng { get; set; }

        public TaxiServiceStatus ServiceStatus { get; set; }
        public TaxiOutServiceStatus OutServiceStatus { get; set; }
        #endregion

        #region Psgn updates
        public List<BroadcastJobInfo> JobList { get; set; }
        #endregion

    }

    public class RegisteredDriver
    {
        public int ID { get; set; }
    }
    
}
