﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StackExchange.Redis;
using GTaxi.Helper;

namespace GTaxi.Taxi.ApiServer
{
    public class RedisConnector
    {
        public static ConnectionMultiplexer RedisServer { get; set; } = null;

        public static void Init()
        {
            try
            {
                RedisServer = ConnectionMultiplexer.Connect("localhost");
            }
            catch
            {
                ConsoleLog.WriteLog("RedisConnector", "RedisServer init failed.", ConsoleColor.Red);
            }
        }

        public static IDatabase GetDatabase()
        {
            if (RedisServer == null) throw new Exception("Redis server is unavailable.");
            return RedisServer.GetDatabase();
        }
    }
}
