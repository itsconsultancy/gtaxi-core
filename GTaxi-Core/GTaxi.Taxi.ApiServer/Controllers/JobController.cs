﻿using ITS.Server.Infrast.AttrExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace GTaxi.Taxi.ApiServer.Controllers
{
    [AllowCrossSiteJson]
    [RoutePrefix("job")]
    public class JobController : ApiController
    {
        [HttpGet]
        [Route("get/{jobID:int}")]
        public HttpResponseMessage GetJob(int jobID)
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpGet]
        [Route("ack/{jobID:int}")]
        public HttpResponseMessage AckJob(int jobID)
        {
            return Request.CreateResponse(HttpStatusCode.OK);
        }

    }
}
