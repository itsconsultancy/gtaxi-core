﻿using GTaxi.Taxi.ApiServer.Caching;
using ITS.Server.Infrast.AttrExtensions;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

using GTaxi.Core.Definitions;
using GTaxi.Core.Models.TaxiApi;
using GTaxi.Helper;

namespace GTaxi.Taxi.ApiServer.Controllers
{
    public static class SignInExtensions
    {
        public static RegisteredTaxi ToRegisteredTaxi(this SignInRequest request)
        {
            return new RegisteredTaxi()
            {
                TaxiID = request.TaxiID,
                DriverID = request.DriverID,
                JobID = request.JobID,
                Lat = request.Lat,
                Lng = request.Lng,
                ServiceStatus = request.ServiceStatus,
                OutServiceStatus = request.OutServiceStatus
            };
        }
    }
    
    [AllowCrossSiteJson]
    [RoutePrefix("taxi")]
    public class TaxiController : ApiController
    {
        private static int count = 1;

        [HttpPost]
        [Route("signin")]
        public HttpResponseMessage SignIn(SignInRequest request)
        {
            ConsoleLog.WriteLog("TaxiController", "Called..." + (count++).ToString(), ConsoleColor.Yellow);

            // cast to service-known object
            //var taxi = request.ToRegisteredTaxi();

            // update to db
            //ServerCaching.UpdateRegisteredTaxi(taxi);

            // get broadcast job from db
            //var broadcastJobList = ServerCaching.GetBroadcastJob(taxi);

            var a = RedisConnector.GetDatabase();

            return Request.CreateResponse(HttpStatusCode.OK, new SignInResponse() { JobList = broadcastJobList });
        }

        [HttpPost]
        [Route("signin-test")]
        public HttpResponseMessage SignInTest(SignInRequest request)
        {
            // Test
            var broadcastJobList = new List<BroadcastJobInfo>()
            {
                new BroadcastJobInfo()
                {
                    JobID = 1,
                    CreatedTime = DateTime.Now,
                    PickLat = 13.2345,
                    PickLng = 100.6789,
                    PickLocation = "สนามบินดอนเมือง",
                    PickLandmark = "ใกล้ร้านกาแฟ ทางออก 1",
                    PickExpectedTime = DateTime.Now.AddHours(1),
                    DestLat = 13.1123,
                    DestLng = 101.2319,
                    DestLocation = "ศูนย์หนังสือจุฬา",
                    DestExpectedTime = null
                },
                new BroadcastJobInfo()
                {
                    JobID = 2,
                    CreatedTime = DateTime.Now,
                    PickLat = 13.2345,
                    PickLng = 100.6789,
                    PickLocation = "เซ็นทรัลพระราม 9",
                    PickLandmark = "ใกล้ทางออก MRT ฝั่งฟอร์จูน",
                    PickExpectedTime = DateTime.Now.AddHours(.5),
                    DestLat = 13.1123,
                    DestLng = 101.2319,
                    DestLocation = "กรมทางหลวง",
                    DestExpectedTime = DateTime.Now.AddHours(1)
                }
            };

            return Request.CreateResponse(HttpStatusCode.OK, new SignInResponse() { JobList = broadcastJobList });
        }
    }
}
