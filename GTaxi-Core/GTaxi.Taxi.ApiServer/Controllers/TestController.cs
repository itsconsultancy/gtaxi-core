﻿using GTaxi.Helper;
using ITS.Server.Infrast.AttrExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace GTaxi.Taxi.ApiServer.Controllers
{
    [AllowCrossSiteJson]
    [RoutePrefix("test")]
    public class TestController : ApiController
    {
        private static int i = 0;
        private static Random rand = new Random();
        private static Object sync = new Object();

        [Route("{id}")]
        [HttpGet]
        public HttpResponseMessage Test(int id)
        {

            lock (sync)
            {
                i++;
                ConsoleLog.WriteLogSync("In", id.ToString(), ConsoleColor.Yellow);
            }

            System.Threading.Thread.Sleep(90 * 1000);

            return Request.CreateResponse(HttpStatusCode.Unauthorized, id);
        }
    }
}
