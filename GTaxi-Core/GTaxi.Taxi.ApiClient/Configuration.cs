﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Taxi.ApiClient
{
    public class Configuration
    {
        public static string ApiProtocol { get; set; } = "http";
        public static string ApiEndPoint { get; set; } = "localhost";
        public static int ApiPort { get; set; } = 8080;

        public static string GetApiURL()
        {
            return string.Format("{0}://{1}:{2}", ApiProtocol, ApiEndPoint, ApiPort);
        }
    }
}
