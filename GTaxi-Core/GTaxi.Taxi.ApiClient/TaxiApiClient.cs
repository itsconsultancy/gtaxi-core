﻿using GTaxi.Core.Models.TaxiApi;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GTaxi.Taxi.ApiClient
{
    public class TaxiApiClient
    {
        public static SignInResponse SignIn(SignInRequest data)
        {
            var client = new RestClient(Configuration.GetApiURL());
            var request = new RestRequest("taxi/signin", Method.POST).AddJsonBody(data);
            RestResponse<SignInResponse> response = (RestResponse<SignInResponse>)client.Execute<SignInResponse>(request);
            return response.Data;
        }
    }
}
