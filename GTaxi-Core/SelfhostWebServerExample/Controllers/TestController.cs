﻿using SelfhostWebServerExample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace SelfhostWebServerExample.Controllers
{
    /// <summary>
    /// รูปแบบ URL จะเป็น
    /// [HostEndPoint] + [RoutePrefix] + [Route]
    /// โดย 
    /// HostEndPoint ตั้งค่าใน Program.cs
    /// ====
    /// ตัวอย่าง
    /// http://localhost:8080/test/get
    /// </summary>
    [RoutePrefix("test")] // คำขึ้นต้นเพื่อเป็น Head Uri สำหรับ Controller นี้ (ตามมาตรฐานควรเป็นชื่อ Controller เช่น GpsController ควรใช้ Prefix ว่า "Gps" เป็นให้ง่ายต่อการแยกชุด Api)
    [AllowCrossSiteJson] // ตรงนี้ไม่ต้องสนใจก็ได้ เอาไว้ "allow another domain to request json in this domain"
    public class TestController : ApiController
    {
        // URL จะเป็น pattern นี้คือ :: http://localhost:8080/test
        [Route("")] // Route
        [HttpGet] // รูปแบบ HTTP Request ของ Api นี้
        public string GetSomeString()
        {
            return "Fuck, yeah!!";
        }

        // URL จะเป็น pattern นี้คือ :: http://localhost:8080/test/get
        [Route("get")] // Route
        [HttpGet] // รูปแบบ HTTP Request ของ Api นี้
        public int GetSomeNumber()
        {
            return 1;
        }

        // URL จะเป็น pattern นี้คือ :: http://localhost:8080/test/set-50-13.2341-100.23234
        [Route("set-{taxiID:int}-{lat:double}-{lng:double}")] // Route : {taxiID:int} === taxiID เป็น parameter, int เป็น type
        [HttpGet] // รูปแบบ HTTP Request ของ Api นี้
        public string GetSomeNumber(int taxiID, double lat, double lng)
        {
            return string.Format("TaxiID: {0}, LatLngLocation: {1},{2}", taxiID, lat, lng);
        }

        // URL จะเป็น pattern นี้คือ :: http://localhost:8080/test/get/toyota/20
        [Route("get/{provider}/{taxiID:int}")]
        [HttpGet] // รูปแบบ HTTP Request ของ Api นี้
        public TestResponse GetSomeNumber(int taxiID, string provider)
        {
            return new TestResponse()
            {
                ID = taxiID,
                SomeData = new List<string>()
                {
                    provider, "ABC", "XYZ", "123"
                }
            };
        }
        
    }
}
