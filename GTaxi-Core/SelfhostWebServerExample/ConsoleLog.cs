﻿using System;

namespace SelfhostWebServerExample
{
    public class ConsoleLog
    {
        public static void WriteLog(string processName, string message, ConsoleColor messageColor)
        {
            Console.Write(DateTime.Now.ToString("HH:mm:ss") + " ");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(processName);
            Console.ForegroundColor = messageColor;
            Console.WriteLine(" " + message);
            Console.ResetColor();
        }

    }
}
