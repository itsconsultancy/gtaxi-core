﻿using Owin;
using System.Web.Http;

namespace SelfhostWebServerExample
{
    public class Startup
    {
        // This method is required by Katana:
        public void Configuration(IAppBuilder app)
        {
            var webApiConfiguration = ConfigureWebApi();

            // Use the extension method provided by the WebApi.Owin library:
            app.UseWebApi(webApiConfiguration);
        }

        // Routing Configuration
        private HttpConfiguration ConfigureWebApi()
        {
            var config = new HttpConfiguration();

            // Template routing
            config.Routes.MapHttpRoute(
                "DefaultApi",
                "api/{controller}");

            //  Enable attribute based routing
            config.MapHttpAttributeRoutes();

            return config;
        }
    }
}
