﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfhostWebServerExample.Models
{
    /// <summary>
    /// ข้อมูลที่เป็น input เข้าสู่ Api
    /// ***** สำหรับสิ่งที่เป็น HTTP Get ไม่ต้องนิยาม input ดังนั้นจึง commend ออกไม่ใช้
    /// ***** เนื่องจากข้อมูลที่ส่งมาจะอยู่ใน URL อยู่แล้ว ให้ดูที่ตัวอย่างใน TestController.cs
    /// ***** แต่ถ้าเป็นแบบ HTTP Post จำเป็นต้องนิยาม input เป็น class แบบนี้
    /// </summary>
    //public class TestRequest
    //{
    //    public int TaxiID { get; set; }
    //    public int DriverID { get; set; }
    //}
}
