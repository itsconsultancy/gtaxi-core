﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfhostWebServerExample.Models
{
    /// <summary>
    /// ข้อมูลที่เป็น return ออกจาก Api
    /// </summary>
    public class TestResponse
    {
        public int ID { get; set; }
        public List<string> SomeData { get; set; }
    }
}
