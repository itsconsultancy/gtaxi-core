﻿using Microsoft.Owin.Hosting;
using System;
using System.Threading.Tasks;

namespace SelfhostWebServerExample
{
    class Program
    {
        // เซตค่า Server EndPoint
        static string serverProtocol = "http";
        static string serverEndPoint = "localhost";
        static int serverPort = 8080;

        // เซตค่าทั่วไป
        static string consoleTitle = "Example Server";

        static void Main(string[] args)
        {
            // Server base address
            string baseAddress = string.Format("{0}://{1}:{2}/", serverProtocol, serverEndPoint, serverPort);

            // Run server
            Console.Title = consoleTitle + " : " + serverPort.ToString();
            ConsoleLog.WriteLog("Server", "Starting web Server...", ConsoleColor.White);
            try
            {
                // Start server
                WebApp.Start<Startup>(baseAddress);
            }
            catch (Exception ex)
            {
                ConsoleLog.WriteLog("Server", ex.ToString(), ConsoleColor.Red);
                Console.ReadKey();
                Environment.Exit(0);
            }

            ConsoleLog.WriteLog("Server", "Server running at " + baseAddress, ConsoleColor.Green);
            Console.ReadLine();
        }
    }
}
