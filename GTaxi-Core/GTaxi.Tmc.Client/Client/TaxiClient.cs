﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GTaxi.Core.Models.Api.Tmc;
using GTaxi.Core.Definitions;
namespace GTaxi.Tmc.Client
{
    public class TaxiClient
    {
        private Random rand = new Random();
        
        public IEnumerable<TaxiInfo> GetTaxisInfo()
        {
            List<TaxiInfo> taxisInfo = new List<TaxiInfo>();
            for(int i = 0; i < 500; i++)
            {
                taxisInfo.Add(new TaxiInfo()
                {
                    TID = i + 1,
                    DID = i + 1,
                    JID = null,
                    FName = "",
                    LName = "",
                    PhoneNO = "",
                    Gender = rand.Next(0, 3) == 0 ? Gender.Male : Gender.Female,
                    ImgURL = "",
                    Lat = rand.NextDouble() * 13,
                    Lng = rand.NextDouble()
                });
            }
            return taxisInfo;
        }
    }
}
